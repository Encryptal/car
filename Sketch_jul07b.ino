const int EnableL = 10; // left side
const int HighL = 9;
const int LowL = 8;


const int EnableR = 7; // Right side
const int HighR = 6;
const int LowR = 5;

void setup() {
  // put your setup code here, to run once:
  pinMode(EnableL,OUTPUT);
  pinMode(HighL,OUTPUT);
  pinMode(LowL,OUTPUT);
  Serial.begin(9600);
  
  pinMode(EnableR,OUTPUT);
  pinMode(HighR,OUTPUT);
  pinMode(LowR,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  
  digitalWrite(HighR, LOW);
  digitalWrite(LowR, HIGH);
  analogWrite(EnableR, 255);
  digitalWrite(HighL, LOW);
  digitalWrite(LowL, HIGH);
  analogWrite(EnableL, 255);
  Serial.println("Go");
}